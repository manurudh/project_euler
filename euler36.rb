beginning = Time.now

def binary(number)
	return number.to_s(2)
end

sum = 0
(1..1000000).each do |num|
	if num.to_s == num.to_s.reverse
		if binary(num) == binary(num).reverse
			sum+=num
		end
	end
end

puts sum
puts "Time elapsed #{Time.now - beginning} seconds."