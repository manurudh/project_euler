beginning = Time.now

def prime(number)
	if number==0 || number==1 || number==2
		return true
	else
		max_divisor = Math.sqrt(number.abs).ceil
		(2..max_divisor).each do |divisor|
			if number%divisor == 0
				return false
			end
		end
		return true
	end
end

def circular_prime(number, prime_list)
	if number>10 && number.to_s.split('').include?(['0','2','4','5','6','8'])
		return false
	elsif number>3 && (((number.to_s.split('')).map! {|i| i.to_i}).reduce(:+))%3==0
		return false
	elsif !prime(number)
		return false
	else
		str = number.to_s
		perm = (str.chars.to_a.permutation.map(&:join)).uniq
		perm.map! {|i| i.to_i}
		perm.each do |num|
			if !prime(num)
				return false
			end
		end
		return true
	end
end


counter = 0
(2..1000000).each do |num|
	if circular_prime(num)
		counter+=1
	end
end	

puts counter

puts "Time elapsed #{Time.now - beginning} seconds."