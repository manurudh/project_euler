beginning = Time.now

names = File.read("euler22.txt").split('","').sort
val = {}
number = 0
('A'..'Z').each do |letter|
	number +=1
	val.merge!(letter => number)
end

def name_value(name, val)
	array = name.split('').map {|i| val[i]}
	array.reduce(:+)
end


index = 1
name_scores = []
names.each do |name|
	name_scores.push(name_value(name, val)*index)
	index+=1
end

puts name_scores.reduce(:+)
puts "Time elapsed #{Time.now - beginning} seconds."