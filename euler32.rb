beginning = Time.now

# equation of form ab x cde = fghi
def pandigital(ab, cde)
	fghi = ab*cde
	pan = []
	pan.concat ab.to_s.split('')
	pan.concat cde.to_s.split('')
	pan.concat fghi.to_s.split('')
	pan.uniq!
	if pan.length == 9
		return true
	else
		return false
	end
end

num = []
(1..70).each do |ab|
	(1..3100).each do |cde|
		temp = (ab*cde).to_s.split('')
		if !ab.to_s.split('').include?('0') && !cde.to_s.split('').include?('0')
			if temp.length == 4 && !temp.include?('0')
				if pandigital(ab, cde)
					puts "#{ab} x #{cde} = #{ab*cde}"
					num.push(ab*cde)
				end
			end
		end
	end
end

num.uniq!
puts num.reduce(:+)
puts "Time elapsed #{Time.now - beginning} seconds."