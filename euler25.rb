beginning = Time.now

def fibonacci(f1, f2)
	f1+f2
end

f1=1; f2 =1; temp = 0; count = 1

while f1.to_s.length < 1000
	temp = fibonacci(f1, f2)
	f1 = f2
	f2 = temp
	count+=1
end

puts "The first term to contain 1000 digits: #{count}."
puts "Time elapsed #{Time.now - beginning} seconds."