beginning = Time.now

year = 1900
month =1
day = 0 #Week from M-Su, where Su is day 7.
counter = 0 # Counts Sundays on the first of the month.

def days_in_month(year, month)
	if [4,6,9,11].include?(month)
		days = 30
	elsif [1,3,5,7,8,10,12].include?(month)
		days = 31
	else
		if year%400 == 0
			days = 29
		elsif year%100 != 0 && year%4 == 0
			days = 29
		else
			days = 28
		end
	end
	return days
end

# Increases counter for every Su on month 1st from 1900.
(1900..2000).each do |year|
	(1..12).each do |month|
		(1..days_in_month(year,month)).each do |date|
			day+=1
			if date == 1 && day%7 == 0
				counter +=1
			end 
		end
	end
end

# Reduces counter for every Su on month 1st in 1900. 
(1..12).each do |month|
		(1..days_in_month(year,month)).each do |date|
			day+=1
			if date == 1 && day%7 == 0
				counter -=1
			end 
		end
	end
	
puts counter
puts "Time elapsed #{Time.now - beginning} seconds."

