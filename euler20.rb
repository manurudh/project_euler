beginning = Time.now

class Integer
  def factorial
    (1..self).reduce(:*) || 1
  end
end

fact = 100.factorial.to_s.split('')

puts fact.map(&:to_i).reduce(:+)
puts "Time elapsed #{Time.now - beginning} seconds."
	