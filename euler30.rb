beginning = Time.now

num = []

(2..1000000).each do |i|
	temp = i.to_s.split('')
	temp2 = temp.map {|j| (j.to_i)**5}
	temp3 = temp2.reduce(:+)
	if temp3 == i
		num.push(temp3)
	end
end

puts num.reduce(:+)
puts "Time elapsed #{Time.now - beginning} seconds."