beginning = Time.now

def prime(number)
	if number==0 || number==1 || number==2
		return true
	else
		max_divisor = Math.sqrt(number.abs).ceil
		(2..max_divisor).each do |divisor|
			if number%divisor == 0
				return false
			end
		end
		return true
	end
end

# function of form n^2 + an + b
def funct(a, b)
	n=0
	while prime(n**2+a*n+b)
		n+=1
	end
	return n
end

coef_a = 0; coef_b = 0; max_primes = 0
(-999..999).each do |a|
	(-999..999).each do |b|
		primes = funct(a, b)
		if primes > max_primes
			max_primes = primes
			coef_a = a
			coef_b = b
		end
	end
end

puts "Max primes: #{max_primes}, a= #{coef_a}, b= #{coef_b}."
puts "a*b= #{coef_b*coef_a}."
puts "Time elapsed #{Time.now - beginning} seconds."