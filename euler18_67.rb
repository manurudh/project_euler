beginning = Time.now

triangle = []

File.open("euler18.txt", "r") do |f|
  f.each_line do |line|
    triangle.push(line)
  end
end

index = triangle.length-1
a1 = triangle[index].to_s.split(" ")
a2 = triangle[index-1].to_s.split(" ")
sum_array = []
(0..a2.length()-1).each do |i|
	sum_array.push([(a2[i].to_i+a1[i].to_i), (a2[i].to_i+a1[i+1].to_i)].max)
end
index-=2

(0..triangle.length()-2).each do |j|
	a1 = sum_array
	a2 = triangle[index].to_s.split(" ")
	sum_array = []

	(0..a2.length()-1).each do |i|
		sum_array.push([(a2[i].to_i+a1[i].to_i), (a2[i].to_i+a1[i+1].to_i)].max)
	end

	index-=1
end

puts a1
puts "Time elapsed #{Time.now - beginning} seconds."